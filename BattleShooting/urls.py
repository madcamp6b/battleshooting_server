
from django.conf.urls import patterns, include, url
from django.contrib import admin
import BattleShooting.game.views
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BattleShooting.views.home', name='home'),
    # url(r'^BattleShooting/', include('BattleShooting.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     (r'^admin/', admin.site.urls),
     (r'^login/$', BattleShooting.game.views.login),
     (r'^logout/$',BattleShooting.game.views.logout),
     (r'^signup/$', BattleShooting.game.views.signUp),
     (r'^users/get/$',BattleShooting.game.views.getUser),
     (r'^rooms/get/$', BattleShooting.game.views.getRooms),
     (r'^rooms/join/$', BattleShooting.game.views.joinRoom),
     (r'^rooms/register/$', BattleShooting.game.views.registerRoom), 
     (r'^players/update/$', BattleShooting.game.views.updatePlayer),
     (r'^players/enemy/$', BattleShooting.game.views.getEnemyPlayer),
     (r'^players/timeLine/update/$', BattleShooting.game.views.updateEnemyTimeLine),
     (r'^players/timeLine/get/$', BattleShooting.game.views.getTimeLine),
     (r'^players/timeLine/show/$', BattleShooting.game.views.showTimeLines),

)
