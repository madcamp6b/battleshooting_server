# -*- coding: utf-8 -*-
from BattleShooting.game.models import User, Room, Player
from django.core import serializers
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import logging



logger = logging.getLogger('BattleShooting.game')

timeSet = {} #여기에다가(id : timeLine) pair를 저장


    
@csrf_exempt
def updateEnemyTimeLine(request):
    params = request.POST
    timeLineData = params.get('timeLine')
    playerId = params.get('playerId')
    stage = params.get('stage')
    logger.debug(timeLineData)
    logger.debug(playerId)
    logger.debug(stage)
    selfPlayer = Player.objects.get(id = playerId)
    selfUser = selfPlayer.user;
    enemy = selfUser.belongingRoom.user_set.exclude(id=selfUser.id)
    enemyPlayer  = Player.objects.get(user = enemy[0])
    timeSet[str(enemyPlayer.id)+str(stage)] = timeLineData

    logger.debug('update compl')

    return HttpResponse(timeSet[str(enemyPlayer.id)+str(stage)])

@csrf_exempt
def getTimeLine(request):
    params = request.POST
    playerId = params.get('playerId')
    stage = params.get('stage')
    logger.debug(playerId)
    logger.debug(stage)
    if timeSet.has_key(str(playerId)+str(stage)):
        logger.debug("send Timeset")
        logger.debug(timeSet[str(playerId)+str(stage)] )
        return HttpResponse(timeSet[str(playerId)+str(stage)])
    else:
        logger.debug('failed to send Timeset')
        return HttpResponse(-1)

@csrf_exempt
def showTimeLines(request):
    params = request.GET
    playerId = params.get('playerId')
    stage = params.get('stage')
    logger.debug(playerId)
    logger.debug(timeSet)
    return HttpResponse(str(timeSet))

@csrf_exempt
def signUp(request):
    if(request.method == 'POST'):
        logger = logging.getLogger('BattleShooting.game')
        
        params = request.POST
        userId = params.get('id','NONE')
        
        logger.debug(userId+" ")
        if(len(User.objects.filter(uid = userId)) != 0):
            return HttpResponse('existed')
        passwd = params.get('passwd', 'NONE')
        nName = params.get('nickname', 'NONE')
       
        logger.debug(userId+" "+passwd+" "+nName)
        
        user = User(uid = userId, password = passwd, nickname = nName, state = 'logout')
        user.save()
        return HttpResponse("created")
    return HttpResponse('get')

@csrf_exempt
def login(request):
    if(request.method == 'POST'):
        logger = logging.getLogger('BattleShooting.game')
        
        
        params = request.POST
        userId = params.get('id','NONE')
        passwd = params.get('passwd', 'NONE')
        state = params.get('state','NONE')
        user = []
        if(state == 'logout'):
            user = User.objects.filter(uid = userId, password = passwd)
            logger.debug('logout')
        else:
            logger.debug('login')
            user = User.objects.filter(uid = userId)

        logger.debug(userId+" "+passwd)

        #user = User.objects.get(uid = userId)
        if(len(user) != 0):
            user[0].state = 'login'
            user[0].save()
            logger.debug(""+str(len(user)))
            return HttpResponse(user[0].id)
    return HttpResponse(-1)

@csrf_exempt
def logout(request):
    if(request.method == 'POST'):
        params = request.POST
        userId = params.get('id', 'NONE')
        
        user = User.objects.get(uid = userId)
        user.state = 'logout'
        user.save()
        return HttpResponse('logout')
    return HttpResponse('')

@csrf_exempt
def getUser(request):
    params = request.POST
    userId = int(params.get('id','-1'))
    
    users = ''
    if(userId == -1):
        users = User.objects.all()
    else:
        users = User.objects.filter(id = userId)
    if(len(users) < 1 ):
        return HttpResponse('not exitst')
    else:
        return HttpResponse(serializers.serialize('json', users))
    
@csrf_exempt
def registerRoom(request):
    params = request.POST
    roomTitle = params.get('roomTitle', 'roomTitle')
    enjoyedUser = params.get('uid', 'uid')
    if(len(Room.objects.filter(title=roomTitle))>=1):
        return HttpResponse("existed")
    room = Room(title = roomTitle, state = 'waiting')
    room.save()
    user = User.objects.get(uid=enjoyedUser)
    user.belongingRoom = room
    user.save()
    return HttpResponse(room.id)       #이때 새로 추가된 id가 오나 <-온다 
@csrf_exempt
def getRooms(request):
    params = request.POST
    index = (int(params.get('index','-1')))
    logger.debug(index)
    rooms = ''
    if(index == -1): #return all
        rooms = Room.objects.all()
    else: #return indexed one
        rooms = Room.objects.filter(id=index)
    data = serializers.serialize('json', list(rooms)+list(User.objects.filter(state="login")))
    return HttpResponse(data)
@csrf_exempt        
def joinRoom(request):
    logger = logging.getLogger('BattleShooting.game')
    logger.debug("join")
    params = request.POST
    
    roomId = params.get('roomId')
    userId = params.get('userId')
    logger.debug(roomId + " " + userId)
   
    room = Room.objects.get(id=roomId)
    user1 = User.objects.get(id=userId)
    

    logger.debug(user1.id)
    if(len(room.user_set.all()) >=2):
        return HttpResponse("exceed")
    
    user1.belongingRoom = room
    user1.save()

    logger.debug(user1.id)
    player = Player(weapon=0, skill=0, score=0, state="waiting", user=user1)
    logger.debug(player.user.id)
    player.save()
    #이밖에 player모델이 생성되는둥 해야됨, 그건 합칠때
    logger.debug('asdasd')
    return HttpResponse(player.id)

@csrf_exempt
def getEnemyPlayer(request):
    params = request.POST
    userId = params.get('id','NONE')
    user1 = User.objects.get(id=userId)
    logger.debug(user1.belongingRoom.user_set.exclude(id=userId))
    enemy = user1.belongingRoom.user_set.exclude(id=userId)
    if len(enemy) < 1:
        return HttpResponse('asdf')
    else:
        logger.debug("asdfasdfasdfasdfasdf")
        logger.debug(enemy[0])
        logger.debug(Player.objects.filter(user = enemy[0]))
        return HttpResponse(serializers.serialize('json',Player.objects.filter(user = enemy[0])))

@csrf_exempt
def updatePlayer(request):
    params = request.POST
    userId = params.get("userId", -1)
    playerId = params.get("playerId", -1)
    pWeapon = params.get("weapon", -1)
    pSkill = params.get("skill", -1)
    pScore = params.get("score", -1)
    pState = params.get("state", "NONE") #playing, waiting, ready, deleting,
    logger.debug(pState)

    player = ''
    if int(playerId) == -1:
        user1 = User.objects.get(id = userId)
        player = Player(weapon=pWeapon, skill=pSkill, score=pScore, state=pState, user=user1)
        player.save()
        return HttpResponse(player.id)
    else:
        player = Player.objects.get(id = playerId)
        player.score = pScore
        player.weapon = pWeapon
        player.skill = pSkill
        player.state = pState
        logger.debug("player save ")
        if player.state == "deleting":
            player.delete()
            player.user.belongingRoom = None
            return HttpResponse('delete')
        else :
            player.save()
            data = serializers.serialize('json',Player.objects.filter(id=playerId))
            return HttpResponse(data)




    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        