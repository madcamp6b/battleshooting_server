from django.contrib import admin
from django.db import models


class User(models.Model):
    uid = models.CharField(max_length=20, null = False)
    password = models.CharField(max_length=20, null = False)
    nickname = models.CharField(max_length=20, null = False)
    belongingRoom = models.ForeignKey('Room', null = True)
    state = models.CharField(max_length=10, null = False) #is logged in or not
    def __unicode__(self):
        return self.uid + " " + self.nickname
class UserManager(admin.ModelAdmin):
    fields = ['uid', 'password', 'nickname']
class Room(models.Model):
    state = models.CharField(max_length=10, null = False)
    title = models.CharField(max_length=30, null = False)
    def __unicode__(self):
        return self.title
admin.site.register(Room)
class Player(models.Model):
    user = models.ForeignKey(User)
    weapon = models.PositiveIntegerField(null = True)
    skill = models.PositiveIntegerField(null = True)
    score = models.PositiveIntegerField(default=0, null = False)
    state = models.CharField(max_length=10, null = False)

    
admin.site.register(Player)
admin.site.register(User, UserManager)